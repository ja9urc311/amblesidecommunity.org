# By using the symbol ':user' we get Factory Girl to simulate the User model.
Factory.define :user do |user|
  user.email                  'user@example.org'
  user.password               'barbaz'
  user.password_confirmation  'barbaz'
end

Factory.define :resident do |resident|
  resident.last_name      'user'
  resident.first_name     'test'
  resident.phone_number   '3602223333'
end

Factory.sequence :email do |n|
  "person-#{n}@example.com"
end
