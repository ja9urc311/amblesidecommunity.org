require 'spec_helper'

=begin
describe "Friendly Forwarding" do
  it 'should forward to the requested page after signin' do
    user = Factory(:user, :resident => Factory(:resident))
    visit edit_user_path(user)
    fill_in :email,     :with => user.email
    fill_in :password,  :with => user.password
    fill_in 'Last name', :with => user.resident.last_name
    fill_in 'First name', :with => user.resident.first_name
    fill_in 'Phone number', :with => user.resident.phone_number
    click_button
    response.should render_template('users/edit')
  end
end
=end
