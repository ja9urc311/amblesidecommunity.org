require 'spec_helper'

describe "Users" do
  describe 'signup' do
    describe 'success' do
      it 'should make a new user' do
        lambda do
          visit signup_path
          fill_in 'Email',          :with => 'user@example.com'
          fill_in 'Password',       :with => 'foobar'
          fill_in 'Confirmation',  :with => 'foobar'
          fill_in 'Last name',      :with => 'testy'
          fill_in 'First name',      :with => 'test'
          fill_in 'Phone number',   :with => 3608889999
          click_button
          response.should have_selector("div.flash.success",
                                        :content => "Welcome")
          response.should render_template('users/show')
        end.should change(User, :count).by(1)
      end

      it 'should sign a user in and out' do
        user = Factory(:user, :resident => Factory(:resident))
        integration_sign_in(user) 
        controller.should be_signed_in
        visit root_path
        click_link 'Sign out'
        controller.should_not be_signed_in
      end
    end

    describe 'failure' do
      it 'should not make a new user' do
        lambda do
          visit signup_path
          fill_in 'Email',          :with => ''
          fill_in 'Password',       :with => ''
          fill_in 'Confirmation',   :with => ''
          click_button
          response.should render_template('users/new')
          response.should have_selector("div#error_explanation")
        end.should_not change(User, :count)
      end

      it 'should not sign in a user' do
        visit signin_path
        fill_in :email,     :with => ""
        fill_in :password,  :with => ""
        click_button
        response.should have_selector('div.flash.error', :content => 'Invalid')
      end
    end
  end
end
