# == Schema Information
#
# Table name: residents
#
#  id              :integer         not null, primary key
#  residence_id    :integer
#  last_name       :string(255)
#  first_name      :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#  phone_area_code :integer(3)
#  phone_prefix    :integer(3)
#  phone_suffix    :string(4)
#

require 'spec_helper'

describe Resident do
  before(:each) do
    @attr =
    {
      :last_name => "purcell",
      :first_name => "jeff",
      :phone_number => "3604760022" 
    }
  end

  it 'should create a new instance given valid attributes' do
    Resident.create!(@attr)
  end

  it 'should require a last name' do
    Resident.new(@attr.merge(:last_name => '')).
      should_not be_valid
  end

  it 'should reject a long last name' do
    long = 'a' * 16 
    Resident.new(@attr.merge(:last_name => long)).
      should_not be_valid
  end

  it 'should require a first name' do
    Resident.new(@attr.merge(:first_name => '')).
      should_not be_valid
  end

  it 'should reject a long first name' do
    long = 'a' * 16
    Resident.new(@attr.merge(:first_name => long)).
      should_not be_valid
  end

  it 'should require a phone number' do
    Resident.new(@attr.merge(:phone_number => '')).
      should_not be_valid
  end

  it 'should reject a short phone number' do
    Resident.new(@attr.merge(:phone_number => '23')).
      should_not be_valid
  end

  it 'should reject a long phone number' do
    long = 1 * 11
    Resident.new(@attr.merge(:phone_number => long)).
      should_not be_valid
  end

  it 'should reject a non-numeric phone number' do
    Resident.new(@attr.merge(:phone_number => 'a123456789')).
      should_not be_valid
  end

  it 'should reject a number less than 0' do
    Resident.new(@attr.merge(:phone_number => -1234567890)).
      should_not be_valid
  end

  describe 'is_active attribute' do
    before(:each) do
      @resident = Resident.create!(@attr)
    end

    it 'should respond to is_active' do
      @resident.should respond_to(:is_active)
    end

    it 'should not be active by default' do
      @resident.should_not be_is_active
    end

    it 'should be convertible to is_active' do
      @resident.toggle!(:is_active)
      @resident.should be_is_active
    end
  end#is_active attribute

  describe 'user association' do
    before(:each) do
      @resident = Resident.create!(@attr)
      @user_attr = 
      {
        :email => "admin@ambleside.com",
        :passowrd => "foobar",
        :password => "foobar"
      }
    end

    it 'should create a new user given valid attributes' do
      @resident.create_user(@user_attr)
      @resident.user.should be_valid
    end

    it 'should not create a new user given invalid attributes' do
      @resident.create_user(@user_attr.merge(:password => ''))
      @resident.user.should_not be_valid
    end
  end
end
