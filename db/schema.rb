# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120520222449) do

  create_table "assignments", :force => true do |t|
    t.integer  "user_id"
    t.integer  "role_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "assignments", ["role_id"], :name => "index_assignments_on_role_id"
  add_index "assignments", ["user_id", "role_id"], :name => "index_assignments_on_user_id_and_role_id", :unique => true
  add_index "assignments", ["user_id"], :name => "index_assignments_on_user_id"

  create_table "comments", :force => true do |t|
    t.integer  "user_id"
    t.integer  "commentable_id"
    t.string   "commentable_type"
    t.text     "body"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "documents", :force => true do |t|
    t.integer  "user_id",    :null => false
    t.string   "title",      :null => false
    t.string   "url",        :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "documents", ["title"], :name => "index_documents_on_title", :unique => true
  add_index "documents", ["url"], :name => "index_documents_on_url", :unique => true

  create_table "events", :force => true do |t|
    t.integer  "user_id"
    t.string   "name"
    t.string   "location"
    t.datetime "start_at"
    t.datetime "end_at"
    t.text     "description", :limit => 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "positions", :force => true do |t|
    t.integer  "user_id"
    t.integer  "rank",       :null => false
    t.string   "name",       :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "positions", ["name"], :name => "index_positions_on_name", :unique => true
  add_index "positions", ["rank"], :name => "index_positions_on_rank", :unique => true
  add_index "positions", ["user_id"], :name => "index_positions_on_user_id", :unique => true

  create_table "posts", :force => true do |t|
    t.string   "title"
    t.text     "body",       :limit => 255
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "preauths", :force => true do |t|
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "residences", :force => true do |t|
    t.integer  "street_number",              :null => false
    t.text     "street_name",                :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "lot_number",    :limit => 8
  end

  add_index "residences", ["street_number", "street_name"], :name => "index_residences_on_street_number_and_street_name", :unique => true

  create_table "residents", :force => true do |t|
    t.integer  "residence_id"
    t.string   "last_name"
    t.string   "first_name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "phone_area_code", :limit => 3
    t.integer  "phone_prefix",    :limit => 3
    t.string   "phone_suffix",    :limit => 4
  end

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "users", :force => true do |t|
    t.string   "email"
    t.string   "encrypted_password"
    t.string   "salt"
    t.integer  "resident_id"
    t.boolean  "is_active",              :default => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "password_reset_token"
    t.datetime "password_reset_sent_at"
    t.datetime "sign_in"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true

end
