class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.integer :user_id, :null => false
      t.string :title, :null => false
      t.string :url, :null => false

      t.timestamps
    end

    add_index :documents, :title, :unique => true
    add_index :documents, :url, :unique => true
  end
end
