class CreateResidences < ActiveRecord::Migration
  def change
    create_table :residences do |t|
      t.integer :street_number, :null => false
      t.text :street_name, :null => false

      t.timestamps
    end

    add_index :residences, [:street_number, :street_name],
      :unique => true
  end
end
