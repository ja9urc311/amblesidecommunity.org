class CreateResidents < ActiveRecord::Migration
  def self.up
    create_table :residents do |t|
      t.integer :residence_id
      t.string :last_name
      t.string :first_name
      t.integer :phone_number, :limit => 8

      t.timestamps
    end
  end

  def self.down
    drop_table :residents
  end
end
