class AddLotNumberToResidence < ActiveRecord::Migration
  def change
    add_column :residences, :lot_number, :integer, :limit => 8
  end
end
