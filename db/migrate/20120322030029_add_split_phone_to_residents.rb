class AddSplitPhoneToResidents < ActiveRecord::Migration
  def change
    add_column :residents, :phone_area_code, :integer, :limit => 3
    add_column :residents, :phone_prefix, :integer, :limit => 3
    add_column :residents, :phone_suffix, :integer, :limit => 4

    Resident.all.each do |r|
      r.update_attributes!(
        :phone_area_code => r.phone_number.to_s[0,3], 
        :phone_prefix => r.phone_number.to_s[3,3], 
        :phone_suffix => r.phone_number.to_s[6,4])
    end

    remove_column :residents, :phone_number
  end
end
