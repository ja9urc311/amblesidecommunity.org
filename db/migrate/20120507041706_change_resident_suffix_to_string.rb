class ChangeResidentSuffixToString < ActiveRecord::Migration
  def change
    change_column :residents, :phone_suffix, :string, limit: 4
  end
end
