class CreatePositions < ActiveRecord::Migration
  def change
    create_table :positions do |t|
      t.integer :user_id
      t.integer :rank, :null => false
      t.string :name, :null => false

      t.timestamps
    end

    add_index :positions, :user_id, :unique => true
    add_index :positions, :name, :unique => true
    add_index :positions, :rank, :unique => true
  end
end
