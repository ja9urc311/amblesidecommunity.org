class AddIndexToAssignment < ActiveRecord::Migration
  def change
    add_index :assignments, :user_id
    add_index :assignments, :role_id
    add_index :assignments, [:user_id, :role_id], :unique => true
  end
end
