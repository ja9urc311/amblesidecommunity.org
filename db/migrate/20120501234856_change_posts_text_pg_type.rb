class ChangePostsTextPgType < ActiveRecord::Migration
  def up
    change_column :posts, :body, :text 
    change_column :events, :description, :text
  end

  def down
  end
end
