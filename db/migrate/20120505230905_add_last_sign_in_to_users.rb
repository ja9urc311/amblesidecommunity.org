class AddLastSignInToUsers < ActiveRecord::Migration
  def change
    add_column :users, :sign_in, :datetime
  end
end
