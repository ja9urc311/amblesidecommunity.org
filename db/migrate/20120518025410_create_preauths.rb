class CreatePreauths < ActiveRecord::Migration
  def change
    create_table :preauths do |t|
      t.string :email

      t.timestamps
    end
  end
end
