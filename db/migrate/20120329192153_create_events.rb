class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.integer :user_id
      t.string :name
      t.string :location
      t.datetime :start_at
      t.datetime :end_at
      t.string :description

      t.timestamps
    end
  end
end
