module ResidentsHelper
  def gravatar_for(resident, options = { :size => 50 })
    gravatar_image_tag(resident.user.email.downcase, :alt => h(resident.user.email),
                                            :class => 'gravatar',
                                            :gravatar => options)
  end
end
