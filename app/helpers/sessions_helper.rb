module SessionsHelper
  def sign_in(user)
    if params[:remember_me]
      cookies.permanent.signed[:remember_token] = [user.id, user.salt]
      cookies.permanent[:sign_in] = Time.now.to_s
    end
    user.update_attribute(:sign_in, Time.now)
    session[:user] = user
    @current_user = user
  end

  def current_user
    if cookies[:remember_token]
      user = user_from_remember_token
      if cookies[:sign_in] &&
        (Time.now - DateTime.parse(cookies[:sign_in])).to_i >= 60*60
        
        user.update_attribute(:sign_in, Time.now)
        cookies[:sign_in] = Time.now.to_s
      end
      return user
    else
      session[:user]
    end
  end

  def signed_in?
    !current_user.nil?
  end

  def sign_out
    cookies.delete(:remember_token)
    cookies.delete(:sign_in)
    session.delete(:user)
    @current_user = nil
  end
  
  def current_user?(user)
    user == current_user
  end

  def deny_access
    store_location
    redirect_to signin_path, :notice => "Please sign in to access this page."
  end

  def redirect_back_or(default)
    redirect_to(session[:return_to] || default)
    clear_return_to
  end

  private
    def user_from_remember_token
      User.authenticate_with_salt(*remember_token)
    end

    def remember_token
      cookies.signed[:remember_token] || [nil, nil]
    end
    
    def store_location
      session[:return_to] = request.fullpath
    end

    def clear_return_to
      session[:return_to] = nil
    end
end
