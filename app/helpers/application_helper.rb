module ApplicationHelper
  def title
    base_title = "Ambleside"
    if @title.nil?
      base_title
    else
      "#{base_title} - #{@title}"
    end
  end

  def selected
    if @selected.nil?
      ""
    else
      @selected
    end
  end

  def inject_analytics?
    !Rails.env.test? && !Rails.env.development? && !Rails.env.local_development?
  end

end
