module MailerHelper
  def subject
    base_subject = 'Ambleside'
    if @subject.nil?
      base_subject
    else
      "#{base_subject} | #{@subject}"
    end
  end 

  def timestamp(date)
    date.strftime('%m/%d/%Y %l:%M %P')
  end
end
