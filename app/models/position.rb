# == Schema Information
#
# Table name: positions
#
#  id         :integer         not null, primary key
#  user_id    :integer
#  rank       :integer         not null
#  name       :string(255)     not null
#  created_at :datetime
#  updated_at :datetime
#

class Position < ActiveRecord::Base
  attr_accessible :name, :rank
  belongs_to :user

  validates :name, :presence => true,
                   :uniqueness => { :case_sensitive => false }

  validates :rank, :presence => true,
                   :uniqueness => true,
                   :numericality => { :only_integer => true,
                                      :greater_than => 0 }
end
