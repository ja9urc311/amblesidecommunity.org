# == Schema Information
#
# Table name: assignments
#
#  id         :integer         not null, primary key
#  user_id    :integer
#  role_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

class Assignment < ActiveRecord::Base
  attr_accessible :user_id, :role_id

  belongs_to :user
  belongs_to :role

  validates :user_id, :presence => true
  validates :role_id, :presence => true
end
