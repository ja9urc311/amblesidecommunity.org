# == Schema Information
#
# Table name: events
#
#  id          :integer         not null, primary key
#  user_id     :integer
#  name        :string(255)
#  location    :string(255)
#  start_at    :datetime
#  end_at      :datetime
#  description :text(255)
#  created_at  :datetime
#  updated_at  :datetime
#

class Event < ActiveRecord::Base
  attr_accessible :name, :location, :start_at, :end_at, :description

  belongs_to :user
  validates_presence_of :user

  has_many :comments, as: :commentable

  validates :name, :location, :start_at, :end_at, :description, :presence => :true
end
