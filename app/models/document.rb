# == Schema Information
#
# Table name: documents
#
#  id         :integer         not null, primary key
#  user_id    :integer         not null
#  title      :string(255)     not null
#  url        :string(255)     not null
#  created_at :datetime
#  updated_at :datetime
#

class Document < ActiveRecord::Base
  attr_accessible :title, :url
  belongs_to :user
  
  validates :user_id, :title, :url, :presence => true

  validates_uniqueness_of :title, :url, :case_sensitive => false
end
