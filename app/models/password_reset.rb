class PasswordReset
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming

  attr_accessor :password, :password_confirmation

  validates_presence_of :password
  validates :password, :confirmation  => true
  validates :password, :length        => { :within => 6..40 }

  def persisted?
    false
  end
end
