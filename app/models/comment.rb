# == Schema Information
#
# Table name: comments
#
#  id               :integer         not null, primary key
#  user_id          :integer
#  commentable_id   :integer
#  commentable_type :string(255)
#  body             :text
#  created_at       :datetime
#  updated_at       :datetime
#

class Comment < ActiveRecord::Base
  attr_accessible :body, :user_id

  belongs_to :commentable, polymorphic: true
  belongs_to :user

  validates :body, presence: true
end
