# == Schema Information
#
# Table name: residences
#
#  id            :integer         not null, primary key
#  street_number :integer         not null
#  street_name   :text            not null
#  created_at    :datetime
#  updated_at    :datetime
#  lot_number    :integer(8)
#

class Residence < ActiveRecord::Base
 attr_accessible :street_number, :street_name, :residents_attributes, :lot_number
 has_many :residents, :readonly => true, :validate => true
 accepts_nested_attributes_for :residents
 
 validates :street_number, :presence => true,
                           :length => { :within => 1..10 },
                           :numericality => { :only_integer => true,
                                              :greater_than => 0 }  
 validates :street_name,   :presence => true,
                           :length => { :within => 2..50 }

 validates_uniqueness_of :street_name, :scope => :street_number

  before_save do |residence|
    new_name = String::new
    words = residence.street_name.split
    words.each_with_index do |word, index|
      new_name << word.capitalize
      if index != words.length-1
        new_name << ' '
      end
    end
    residence.street_name = new_name
  end

  def self.assign_residence(properties = {}, resident)
    street_number = 0 
    begin
      street_number = Integer(properties[:street_number])
    rescue
      street_number = 0
    end

    residence = Residence.find(:first, :conditions =>
      [ 'lower(street_name) = ? AND street_number = ?',
      properties[:street_name].downcase, 
      street_number])

    if residence.nil?
      residence = resident.build_residence(
        :street_number => street_number, 
        :street_name => properties[:street_name])
        
    else
      resident.residence_id = residence.id
      residence = resident.residence
    end
    
    residence.update_attributes(:lot_number => properties[:lot_number])
    return residence
  end
end
