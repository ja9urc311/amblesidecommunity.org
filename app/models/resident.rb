# == Schema Information
#
# Table name: residents
#
#  id              :integer         not null, primary key
#  residence_id    :integer
#  last_name       :string(255)
#  first_name      :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#  phone_area_code :integer(3)
#  phone_prefix    :integer(3)
#  phone_suffix    :string(4)
#

class Resident < ActiveRecord::Base
  attr_accessible :last_name, :first_name,
    :residence_attributes, :phone_area_code, :phone_prefix, :phone_suffix
  has_one :user, :dependent => :destroy
  belongs_to :residence, :validate => true
  validates_presence_of :residence

  validates :last_name, :first_name, :phone_area_code, :phone_prefix,
    :phone_suffix, :presence => true

  validates :last_name, :first_name, :length => { :within => 1..15 }

  validates :phone_area_code, :phone_prefix,
    :numericality => { :only_integer => true }

  validates :phone_area_code, :phone_prefix, :length  => { :is => 3 }
  validates :phone_suffix, :length => { :is => 4 }

  validates_each :phone_suffix do |record, attr, value|
    record.errors.add(attr, 'must be a number') if value.to_i == 0
  end

  before_save do |user|
    user.last_name = user.last_name.capitalize
    user.first_name = user.first_name.capitalize
  end

end
