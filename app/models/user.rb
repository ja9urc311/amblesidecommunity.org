# == Schema Information
#
# Table name: users
#
#  id                     :integer         not null, primary key
#  email                  :string(255)
#  encrypted_password     :string(255)
#  salt                   :string(255)
#  resident_id            :integer
#  is_active              :boolean         default(FALSE)
#  created_at             :datetime
#  updated_at             :datetime
#  password_reset_token   :string(255)
#  password_reset_sent_at :datetime
#  sign_in                :datetime
#

require 'digest'
class User < ActiveRecord::Base
  attr_accessor :password
  attr_accessible :email, :password, :password_confirmation, :is_active, :resident_attributes
  belongs_to :resident, :validate => true, :dependent => :destroy
  has_many :assignments, :dependent => :destroy
  has_many :roles, :through => :assignments, :uniq => true
  has_one :position, :dependent => :nullify, :readonly => true
  has_many :posts
  has_many :documents
  has_many :comments
  validates_presence_of :resident

  email_regex = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  validates :email, :presence => true,
                    :format => { :with => email_regex },
                    :uniqueness => { :case_sensitive => false },
                    :on => :create

  # Automatically creates the virtual attribute 'password_confirmation'.
  validates :password,  :presence      => true, :on => :create
  validates :password,  :confirmation  => true, :on => :create
  validates :password,  :length        => { :within => 6..40 }, :on => :create

  #validates :resident_id,   :presence  => true

  before_save :encrypt_password, :unless => Proc.new { |u| u.password.blank? }

  # Return true if the user's password matches the submitted password.
  def has_password?(submitted_password)
    encrypted_password == encrypt(submitted_password)
  end

  def self.authenticate(email, submitted_password)
    user = find_by_email(email)
    return nil if user.nil?
    return user if user.has_password?(submitted_password)
  end

  def self.authenticate_with_salt(id, cookie_salt)
    user = find_by_id(id)
    (user && user.salt == cookie_salt) ? user : nil
  end

  def role_symbols
    (roles || []).map { |r| r.name.underscore.to_sym }
  end

  def is_admin?
    role_ids.include?(1)
  end

  def send_email_verification
    generate_token(:password_reset_token)
    save!
    UserMailer.email_verification(self).deliver
  end

  def send_password_reset
    generate_token(:password_reset_token)
    self.password_reset_sent_at = Time.zone.now
    save!
    UserMailer.password_reset(self).deliver
  end

  private
    def generate_token(column)
      begin
        self[column] = SecureRandom.urlsafe_base64
      end while User.exists?(column => self[column])
    end

    def encrypt_password
      self.salt = make_salt unless has_password?(password)
      self.encrypted_password = encrypt(password)
    end

    def encrypt(string)
      secure_hash("#{salt}--#{string}")
    end

    def make_salt
      secure_hash("#{Time.now.utc}--#{password}")
    end

    def secure_hash(string)
      Digest::SHA2.hexdigest(string)
    end
end
