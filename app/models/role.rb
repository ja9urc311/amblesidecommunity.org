# == Schema Information
#
# Table name: roles
#
#  id         :integer         not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Role < ActiveRecord::Base
  attr_accessible :name

  has_many :assignments, :dependent => :destroy
  has_many :users, :through => :assignments, :uniq => true

  validates :name, :presence => true,
                   :uniqueness => { :case_sensitive => false } 
end
