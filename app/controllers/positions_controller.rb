class PositionsController < ApplicationController
  filter_resource_access :collection => [:ahoa, :index]
  def index
    @title = 'All Positions'
    @positions = Position.order('rank asc').all
  end

  def ahoa 
    @title = 'AHOA Officers'
    @users = User.joins("inner join Assignments a on users.id = a.user_id inner join Roles r on a.role_id = r.id")
      .where("r.name = ?", 'board').joins("left outer join Positions p on users.id = p.user_id")
      .order("case when p.rank is null then 1 else 0 end, p.rank, users.email")
  end

  def new
    @title = 'New position'
  end

  def create
    if @position.save
      flash[:success] = 'Position created successfully'
      redirect_to positions_path
    else
      render 'new'
    end
  end

  def edit
    @title = 'Edit position'
  end

  def update
    if @position.update_attributes(params[:position])
      flash[:success] = 'Position updated'
      redirect_to positions_path 
    else
      @title = 'Edit position'
      render 'edit'
    end
  end

  def edit_assignment
    @title = "Edit Assignment"
    user_id = @position.user_id.nil? ? 0 : @position.user_id
    @users = User.joins("left outer join positions on users.id " +
      "= positions.user_id").joins("inner join Assignments a on users.id = a.user_id").
      joins("inner join Roles r on a.role_id = r.id")
      .where("(positions.user_id is null or " +
        user_id.to_s + " = users.id) and users.is_active = ? and r.name = ?",
        true, 'board')
  end

  def update_assignment
    params[:user_id] ||= [] 
    user_id = params[:user_id][0]
    if user_id == '0'
      @position.user_id = nil
    else
      user = User.find(user_id)
      @position.user_id = user.id
    end
    @position.save
    flash[:success] = "Assignment Updated"
    redirect_to positions_path
  end
    
  def destroy
    position = Position.find(params[:id])
    position.destroy
    flash[:success] = "Position destroyed"
    redirect_to positions_path
  end

end
