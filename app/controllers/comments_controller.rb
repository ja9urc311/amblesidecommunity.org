class CommentsController < ApplicationController
  filter_resource_access

  def destroy
    comment = Comment.find(params[:id])
    comment.destroy
    flash[:success] = 'Comment destroyed'
    redirect_to request.referer 
  end

end
