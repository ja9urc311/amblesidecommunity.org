class UsersController < ApplicationController
  include UsersHelper
  filter_resource_access
  before_filter :is_myself, :only => [:destroy, :toggle_active]
  before_filter :go_ssl, :only => [:new, :create, :edit, :update, :verify_email]
  layout :get_layout 
	
  def index
    @title = 'All users'
    @users = User.order("email asc")
  end

  def show
    @title = @user.email
  end

  def new
    @resident = @user.build_resident
    @residence = @resident.build_residence
    @title = "Sign up"
  end

  def create
    if User.count == 0
      @user.role_ids = [1]
      @user.is_active = true
    else
      @user.role_ids = [3]
    end
		
    @resident = @user.build_resident(params[:user][:resident])

    residence_values = params[:user][:resident][:residence]
    if !residence_values.nil?
      @residence = Residence.assign_residence(residence_values,
        @resident) 
    else
      @residence = @resident.build_residence
    end

    if @user.save
      if Preauth.find_by_email(@user.email)
          UserMailer.new_auto_user(@user).deliver
          @user.send_email_verification
          redirect_to root_url, notice: 'Welcome to Ambleside. Please check your email for verification.'
      else
        UserMailer.welcome_email(@user).deliver
        UserMailer.new_user(@user).deliver
        flash[:success] = 'Welcome to Ambleside! Your account is ' +
            'pending approval.'
        redirect_to new_session_path
      end
    else 
      @title = 'Sign up'
      @user.password.clear()
      @user.password_confirmation.clear()
      render 'new'
    end
  end

  def edit
    @resident = @user.resident
    @residence = @resident.residence
    @title = 'Edit user'
  end

  def edit_roles
    @title = 'Edit roles'
  end

  def update
    @resident = @user.resident
    @residence = Residence.assign_residence(
      params[:user][:resident][:residence], 
      @resident)

    resident_values = params[:user][:resident]
    resident_values[:residence_id] = @resident.residence_id
    resident_values[:residence] = @resident.residence.attributes

    if @user.update_attributes(params[:user]) & 
      @user.resident.update_attributes(resident_values)
      flash[:success] = "Profile updated."
      redirect_to @user
    else
      @user.password.clear()
      @user.password_confirmation.clear()
      @title = 'Edit user'
      render 'edit'
    end
  end

  def update_roles
    params[:role_ids] ||= []
    @user.role_ids = params[:role_ids] 
    flash[:success] = "Roles updated"
    redirect_to users_path
  end

  def toggle_active
    if @user.is_active?
      @user.update_attribute(:is_active, false)
    else
      @user.send_email_verification
    end
    redirect_to users_path
  end 

  def verify_email
    user = User.find_by_password_reset_token(params[:code])
    if user
      user.update_attribute(:is_active, true)
      user.update_attribute(:password_reset_token, nil)
      redirect_to new_session_path, notice: 'Your email has been verified. Please sign in.'
    else
      redirect_to root_url, notice: 'There was a problem verifying your email.'
    end
  end

  def destroy
    user = User.find(params[:id])
    user.destroy
    flash[:success] = "User destroyed."
    redirect_to users_path
  end

  private
    def is_myself
      user = User.find(params[:id])
      if user.id == current_user.id
        flash[:error] = "You can't administer yourself"
        redirect_to users_path
      end
    end

    def correct_user
      user = User.find(params[:id])
      redirect_to(root_path) unless current_user?(user) ||
        current_user.admin?
    end

    def not_signed_in
      redirect_to(root_path) unless !signed_in?
    end

    def get_layout
      if['new', 'create'].include? action_name
        'pages_minimal'
      else
        'application'
      end
    end

end
