class EventsController < ApplicationController
  include SessionsHelper
  filter_resource_access
  before_filter :set_selected
  layout 'community' 

  def index
    @title = 'Events'
    @events = Event.order("start_at desc")
  end

  def show
    @title = @event.name
  end

  def new
    @title = 'New Event'
  end

  def create
    @event.user_id = current_user.id

    if @event.save
      flash[:success] = 'Event created successfully'
      redirect_to events_path
    else
      render 'new'
    end
  end

  def edit
    @title = 'Edit ' + @event.name
  end

  def update
    if @event.update_attributes(params[:event])
      flash[:success] = 'Event updated'
      redirect_to events_path
    else
      @title = 'Edit event'
      render 'edit'
    end
  end

  def destroy
    event = Event.find(params[:id])
    event.destroy
    flash[:success] = 'Event destroyed'
    redirect_to events_path
  end 

  private
    def set_selected
      @selected = 'Events'
    end
end
