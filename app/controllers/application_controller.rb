class ApplicationController < ActionController::Base
  protect_from_forgery
  include SessionsHelper
  before_filter { |c| Authorization.current_user = c.current_user }
  before_filter :set_host_from_request, :only => :create
  helper_method :timestamp

  def permission_denied
    flash[:error] = "Sorry, you are not allowed to access that page."
    redirect_to root_url
  end

  def go_ssl
    if Rails.env.production? && request.protocol == "http://"
      redirect_to "https://ambleside.heroku.com" + request.fullpath 
    end
  end

  protected
    def timestamp(date)
      date.strftime('%m/%d/%Y %l:%M %P')
    end

  private
    def set_host_from_request
      ActionMailer::Base.default_url_options[:host] = request.host_with_port
    end
end
