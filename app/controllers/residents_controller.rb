require 'csv'

class ResidentsController < ApplicationController
  include UsersHelper
  before_filter :new_resident_from_params, only: :create
  before_filter :new_resident, only: [:index, :new, :get_csv]
  before_filter :resident_from_id, only: [:edit, :show]
  filter_access_to :all, attribute_check: true
  before_filter :set_selected
  layout 'community'

  def index
    @title = 'All residents'

    @residents = all_residents 
  end

  def get_csv
    @residents = all_residents
    csv_string = CSV.generate do |csv|
      csv << ["Lot#", "Last Name", "First Name", "Email", "Street", "Phone"]

      last_lot = String.new
      @residents.each do |r|
        lot_number = r.residence.lot_number != last_lot ?
          r.residence.lot_number : ''

        csv << [lot_number, r.first_name, r.last_name,
          !r.user.nil? ? r.user.email : '', 
          "#{r.residence.street_number.to_s} #{r.residence.street_name}",
          "(#{r.phone_area_code}) #{r.phone_prefix}-#{r.phone_suffix}"]

          last_lot = r.residence.lot_number
      end
    end

    send_data csv_string, type: 'text/csv; charset=iso-8859-1; header-present',
      disposition: 'attachment; filename=residents.csv'
  end

  def show
    @title = @resident.last_name + ', ' + @resident.first_name
  end

  def new
    @residence = @resident.build_residence
    @title = 'New resident'
  end

  def create
    residence_values = params[:resident][:residence]
    if !residence_values.nil?
      @residence = Residence.assign_residence(residence_values,
        @resident)
    else
      @residence = @resident.build_residence
    end

    resident = params[:resident]
    if @resident.save
      flash[:success] = 'Resident was created successfully'
      redirect_to @resident 
    else
      render 'new'
    end
  end

  def edit
    @residence = @resident.residence
    @title = 'Edit resident'
  end

  def update
    @residence = Residence.assign_residence(
      params[:resident][:residence], @resident)
    
    resident_values = params[:resident]
    resident_values[:residence_id] = @resident.residence_id
    resident_values[:residence] = @resident.residence.attributes  
     
    if @resident.update_attributes(resident_values)
      flash[:success] = "Profile updated."
      redirect_to @resident
    else
      render 'edit'
    end
  end

  def destroy
    resident = Resident.find(params[:id])
		if resident.user.nil? || 
            (resident.user.id != current_user.id)
			resident.destroy
			flash[:success] = "Resident destroyed."
		else
			flash[:error] = "You can't administer yourself."
		end
    redirect_to residents_path
  end

  protected
  def new_resident_from_params
    @resident = Resident.new(params[:resident])
  end

  def new_resident
    @resident = Resident.new
  end

  def resident_from_id
    @resident = Resident.find(params[:id])
  end

  private
    def set_selected
      @selected = 'Residents'
    end

    def all_residents
       Resident.find_by_sql([
      "select residents.*, residences.lot_number
      from residents
      left outer join users
      on residents.id = users.resident_id
      inner join residences
      on residents.residence_id = residences.id
      where users.resident_id is null
      union select residents.*, residences.lot_number
      from residents
      inner join users on residents.id = users.resident_id
      inner join residences
      on residents.residence_id = residences.id
      where users.is_active = ? 
      order by lot_number asc, last_name asc, first_name asc", true])
    end
end
