class DocumentsController < ApplicationController
  filter_resource_access
  before_filter :set_selected
  layout 'community'

  def index
    @documents = Document.order('updated_at desc')
  end

  def new
    @title = 'New Document'
  end

  def edit
    @title = 'Edit Document'
  end

  def create
    @document.user_id = current_user.id

    if @document.save
      flash[:success] = 'Document was successfully created.'
      redirect_to documents_path
    else
      render action: "new"
    end
  end

  def update
    @document.user_id = current_user.id

    if @document.update_attributes(params[:document])
      flash[:success] = 'Document was successfully updated.'
      redirect_to documents_path
    else
      render action: "edit"
    end
  end

  def destroy
    @document = Document.find(params[:id])
    @document.destroy
    flash[:success] = 'Document destroyed'

    redirect_to documents_path
  end

  private
    def set_selected
      @selected = 'Documents'
    end
end
