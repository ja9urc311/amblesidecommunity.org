class PostsController < ApplicationController
  include SessionsHelper
  filter_resource_access
  before_filter :set_selected
  layout 'community'

  def index
    @title = 'Blog'
    @posts = Post.order("updated_at desc")
  end

  def show
    @title = @post.title
    @comment = @post.comments.build
    @comments = Comment.where('commentable_id = ?', @post.id)
  end

  def new
    @title = 'New Post'
  end

  def create
    @post.user_id = current_user.id
     
    if @post.save
      flash[:success] = 'Blog post created successfully'
      redirect_to posts_path
    else
      render 'new'
    end
  end

  def edit
    @title = 'Edit Post'
  end

  def update
    if @post.update_attributes(params[:post])
      flash[:success] = 'Post updated'
      redirect_to posts_path
    else
      @title = 'Edit post'
      render 'edit'
    end
  end

  def add_comment
    comment_attr = params[:comment].merge user_id: current_user.id
    comment = @post.comments.build(comment_attr)

    if comment.save
      UserMailer.new_comment(comment).deliver if !current_user?(@post.user)
      flash[:success] = 'Comment added'
      redirect_to @post
    else
      redirect_to @post
    end
  end

  def destroy
    post = Post.find(params[:id])
    post.destroy
    flash[:success] = 'Post destroyed'
    redirect_to posts_path
  end

  private
    def set_selected
      @selected = 'Blog'
    end
end
