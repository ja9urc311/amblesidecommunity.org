class PagesController < ApplicationController
  filter_access_to :community, :dues

  def home
    @title = "Home"
  end

  def about
    @title = "About"
  end

  def contact
    @title = "Contact"
  end

  def community 
    @title = "Community"
    @posts = Post.order('updated_at desc').limit(10)

    @events = Event.order('start_at desc').
      where('start_at < ? AND end_at > ?',
         Date.today >> 1, Date.today).limit(10)
    
    if @events.length < 10
      @events = Event.order('start_at desc').
        where('end_at > ?', Date.today).limit(10)
    end
    
    @documents = Document.order('updated_at desc').limit(10)

    @selected = 'Community'
    render :layout => "community"
  end

  def dues
    @title = 'Electronic Dues'
    @selected = 'Dues'
    render :layout => 'community'
  end

end
