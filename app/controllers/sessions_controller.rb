class SessionsController < ApplicationController
  before_filter :go_ssl
  layout "pages_minimal"

  def new
    @title = 'Sign in'

    if cookies[:remember_token]
      user = user_from_remember_token
      sign_in user
      redirect_back_or community_path 
    end 
  end

  def create
    user = User.authenticate(params[:session][:email],
                             params[:session][:password])
    if user.nil?
      flash.now[:error] = "Invalid email/password combination."
      @title = 'Sign in'
      params[:session][:password].clear() 
      render 'new'
    elsif !user.is_active?
      flash.now[:error] = "This account is not active."
      @title = 'Sign in'
      params[:session][:password].clear() 
      render 'new' 
    else
      sign_in user
      redirect_back_or community_path 
    end
  end

  def destroy
    sign_out
    redirect_to root_path
  end

end
