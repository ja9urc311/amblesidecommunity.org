class PasswordResetsController < ApplicationController
  before_filter :go_ssl, :only => [:edit, :update]
  layout 'pages_minimal'
  
  def new
  end

  def create
    user = User.find_by_email(params[:email])
    if user
      user.send_password_reset
      redirect_to root_url, :notice => "Email sent with password reset instructions."
    else
      redirect_to root_url, :notice => "Invalid email address."
    end
  end

  def edit
    @user = User.find_by_password_reset_token!(params[:id])
    @pw_reset = PasswordReset.new
  end

  def update
    @user = User.find_by_password_reset_token!(params[:id])
    @pw_reset = PasswordReset.new
    @pw_reset.password = params[:user][:password]
    @pw_reset.password_confirmation = params[:user][:password_confirmation]

    if @user.password_reset_sent_at < 2.hours.ago
      redirect_to new_password_reset_path, 
        :alert => "Password reset has expired."
    elsif @user.update_attributes(params[:user]) &&
      @pw_reset.valid?
      redirect_to root_url, :notice => "Password has been reset."
    else
      render :edit
    end
  end
end
