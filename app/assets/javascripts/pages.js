var slideshow = function()
{
  var images_ = ["//lh5.googleusercontent.com/-wpBsz8K5vgo/T6XA3h7Bg7I/AAAAAAAAABY/9iWZ3gZAL0k/s1000/isabella_green.png",
  "//lh5.googleusercontent.com/-NhCyDNcYPO0/T6XA23EwV2I/AAAAAAAAABM/bOYniIeSU_Y/s960/east_gate.png",
    "//lh4.googleusercontent.com/-8vXKi02WIvU/T6XA16Y30eI/AAAAAAAAAA4/9bhDpE0neYQ/s960/ducks.png",
    "//lh4.googleusercontent.com/-oBTFHfZJfkQ/T6iUPICzYdI/AAAAAAAAAB0/BzS8eNtD4Kg/s800/waterfall.png"]

  var history_ = [];
  var fade_duration_ = 1700;

  function get_random_id()
  {
    return Math.floor((Math.random()*images_.length));
  }

  function search(array, string)
  {
    for(var i = 0; i < array.length; i++)
    {
      if(array[i] === string)
      {
        return 0;
      }
    }
    
    return -1;
  }

  return {
    change : function()
    {
      var id = get_random_id();
      while(search(history_, id) !== -1)
      {
        id = get_random_id();
      }

      var $bg = $('#backgrounds');
      $bg.fadeToggle(fade_duration_, function()
      {
        $bg.css('background-image', "url(" + images_[id] + ")");
      });

      $bg.fadeToggle(fade_duration_, function(){});

      history_.push(id);
      
      if(history_.length  === images_.length)
      {
        var last_id = history_[images_.length-1];
        history_ = [];
        history_.push(last_id);
      }
    },

    start : function()
    {
      $('#images-loading').css('display', 'block');

      var $bg = $('#backgrounds');
      $bg.css('display', 'none');

      var id = get_random_id();

      $bg.css('background-image', "url(" + images_[id] + ")");
      $bg.fadeToggle(fade_duration_, function(){});

      history_.push(id);

      var dfd = $('#preloaded-images').imagesLoaded();
      dfd.progress(function(isBroken, $images, $proper, $broken)
      {
        $loading = $('#images-loading');

        if(!isBroken && $loading.css('display') !== 'none')
        {
          $loading.css('display', 'none');
        }
      });

      dfd.done(function($images)
      {
        setInterval('slideshow.change();', 10000);
      });
    }
  };
}();

(function()
{
  slideshow.start();
})();
