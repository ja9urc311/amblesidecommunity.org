class UserMailer < ActionMailer::Base
  include MailerHelper

  def welcome_email(user)
    @user = user
    @subject = "Welcome!"
    mail :to => user.email, :subject => subject 
  end

  def new_user(user)
    @user = user
    @subject = 'New User Request'
    mail :to => get_board_mail_list, :subject => subject
  end
	
  def new_auto_user(user)
      @user = user
      @subject = 'New Auto User'
      mail :to => get_admin_mail_list, subject: subject
  end

  def status_change(user, status)
    @user = user
    @status = status
    @subject = 'Your Account'
    mail :to => user.email, :subject => subject 
  end

  def password_reset(user)
    @user = user
    @subject = 'Password Reset'
    mail :to => user.email, :subject => subject 
  end

  def email_verification(user)
    @user = user
    @subject = 'Email Verification'
    mail to: user.email, subject: subject
  end

  def new_comment(comment)
    @comment = comment
    @subject = "New Comment on #{comment.commentable.title}"
    mail to: comment.commentable.user.email, subject: subject
  end
	
  private
    def get_board_mail_list
      User.select('distinct users.email').joins(:assignments).
      where('assignments.role_id in (1,2)').map(&:email)
    end

    def get_admin_mail_list
      User.select('users.email').joins(:assignments).
      where('assignments.role_id = 1').map(&:email)
    end
end
