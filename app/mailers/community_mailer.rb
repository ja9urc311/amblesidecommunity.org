class CommunityMailer < ActionMailer::Base
  include MailerHelper
  add_template_helper(MailerHelper)
  default :to => User.all.map(&:email)

  def daily
    @subject = "New/Updated Content"
    @posts = Post.where('updated_at > ?', 1.day.ago).order('updated_at desc')
    @events = Event.where('updated_at > ? or extract(doy from start_at) = ?', 
      1.day.ago, Time.now.yday+2).order('start_at asc')
    @documents = Document.where('updated_at > ?', 1.day.ago).order('updated_at desc')

    if @posts.count > 0 || @events.count  > 0 || @documents.count > 0
      mail :subject => subject  
    end
  end

end
