desc "Send email with posts updated less than 1 day ago"
task :post_notify => :environment do
  puts "#{DateTime.now.to_s} - Sending daily community mail..."
  CommunityMailer.daily.deliver
  puts "#{DateTime.now.to_s} - Done."
end

desc "Delete sessions that are older than 2 weeks"
task :clean_sessions => :environment do
  puts "#{DateTime.now.to_s} - Cleaning out old sessions..."
  rows = ActiveRecord::SessionStore::Session.delete_all(["updated_at < ?", 2.weeks.ago])
  puts "#{DateTime.now.to_s} - #{rows} sessions row(s) deleted."
end
