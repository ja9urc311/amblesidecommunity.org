namespace :db do
  desc 'Fill database with sample data'
  task :populate => :environment do
    Rake::Task['db:reset'].invoke
    admin = User.create!(:email => "example@ambleside.org",
                 :password => 'foobar',
                 :password_confirmation => 'foobar')
    admin.toggle!(:admin)
    99.times do |n|
      email = "example-#{n+1}@ambleside.org"
      password = "password"
      User.create!(:email => email,
                   :password => password,
                   :password_confirmation => password)
    end
  end

  desc 'Fill with events'
  task :make_events => :environment do
    99.times do |n|
      title = "this is test title#{n+1}"
      location = "at #{n+1} location"
      start_at = Time.now + n.days 
      end_at = Time.now + n.days + n.hours
      description = "this is test desc#{n+1}"

      Event.create!(name: title, location: location, start_at: start_at,
        end_at: end_at, description: description, user_id: 1)
    end
  end
end
