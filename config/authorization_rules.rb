authorization do
  role :admin do
    has_permission_on [:residents, :positions, :posts, :events, :documents], :to => [:index, :show,
       :new, :create, :edit, :update, :destroy]
    has_permission_on :positions, :to => [:edit_assignment,
      :update_assignment, :ahoa]
    has_permission_on :posts, to: :add_comment
    has_permission_on :users, :to => [:index, :show, :edit, 
      :update, :toggle_active, :edit_roles, :update_roles, :destroy]
    has_permission_on :pages, :to => [:community, :dues]
    has_permission_on :residents, to: :get_csv
    has_permission_on :comments, to: :destroy
  end
  
  role :board do
    includes :resident
    has_permission_on :positions, :to => [:index, :show,
      :new, :create, :edit, :update, :edit_assignment, 
      :update_assignment, :ahoa,:destroy]
    has_permission_on :residents, :to => [:index, :show, 
      :new, :create]
    has_permission_on :residents, :to => [:edit, :update,
      :destroy] do
      if_attribute :user => { :is_admin? => is { false } }
      if_attribute :user => nil
    end
    has_permission_on :users, :to => [:index, :toggle_active,
      :edit_roles, :updates_roles] do
      if_attribute :is_admin? => is { false }, :id => is_not { user.id }
    end
  end

  role :resident do
    has_permission_on [:posts, :events, :documents], :to => [:new, :create]
    has_permission_on [:residents, :posts, :events, :documents], :to => [:index, :show]
    has_permission_on :residents, :to => [:edit, :update] do
      if_attribute :id  => is { user.id }
    end
    has_permission_on [:posts, :events, :documents], :to => [:edit, :update, :destroy] do
      if_attribute :user_id => is { user.id }
    end
    has_permission_on :posts, to: :add_comment
    has_permission_on :users, :to => [:edit, :update, :show] do
      if_attribute :id => is { user.id }
    end
    has_permission_on :positions, :to => :ahoa
    has_permission_on :pages, :to => [:community, :dues]
    has_permission_on :residents, to: :get_csv
  end

  role :guest do
    has_permission_on :users, :to => [:new, :create, :verify_email]
  end
end
