AmblesidecommunityOrg::Application.routes.draw do
  get "comments/destroy"

  resources :comments, only: :destroy

  resources :documents, except: :show

  resources :events

  match '/posts/:id/add_comment', to: 'posts#add_comment'
  resources :posts
  match '/blog', :to => 'posts#index'
  match '/positions/ahoa', :controller => 'positions',
    :action => 'ahoa', :as => :ahoa
  resources :positions, :except => :show

  match '/residents/get_csv', :controller => 'residents',
    :action => 'get_csv', :as => :get_csv 
  resources :residents

  match '/users/:id/verify_email/:code', :to => 'users#verify_email',
    :as => :verify_email
  match '/users/:id/toggle_active', :to => 'users#toggle_active', 
    :as => :toggle_active
  match '/users/:id/edit_roles', :to => 'users#edit_roles',
    :as => :edit_roles
  match '/users/:id/update_roles', :to => 'users#update_roles'
  match 'positions/:id/edit_assignment', :to => 'positions#edit_assignment',
    :as => :edit_assignment
  match 'positions/:id/update_assignment', :to => 'positions#update_assignment'
  resources :users
  resources :sessions, :only => [:new, :create, :destroy]
  resources :password_resets

  match '/signup',  :to => 'users#new'
  match '/signin',  :to => 'sessions#new'
  match '/signout', :to => 'sessions#destroy'
  match '/contact', :to => 'pages#contact'
  match '/about',   :to => 'pages#about'
  match '/community', :to => 'pages#community'
  match '/dues', :to => 'pages#dues'
  
  root :to => 'pages#home'

  # get "pages/home"
  # get "pages/about"
  # get "pages/contact"

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => "welcome#index"

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
end
